# Materiales de mi docencia en la Universidad de Zaragoza

El presente repositorio tiene como propósito el compartir materiales propios de mi docencia en la [Universidad de Zaragoza](https://unizar.es) en las diferentes asignaturas que he impartido.

Este repositorio tiene material bajo licencia [__Creative Common BY-SA 4.0__](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES): todos los materiales que aquí se encuentran pueden ser compartidos libremente siempre que se cite al autor de los mismos y si se modifican se vuelvan a compartir en una licencia del mismo tipo.

__CC By-SA 2020 Miguel Sevilla-Callejo__

## Máster en Tecnologías de la Información Geográfica y la Ordenación del Territorio

[Enlace a la página del máster de la Universidad de Zaragoza](https://estudios.unizar.es/estudio/ver?id=608)

### Módulo de introducción a QGIS3

- Presentaciones y material de clase disponible en el directorio [qgis_moodle_2020](https://gitlab.com/msevilla00/unizar/-/tree/master/qgis_moodle_2020) de este repositorio.

- Alguno de los archivos de utilidad están en el repositorio propio de QGIS en gitlab:  [https://gitlab.com/msevilla00/qgis_files](https://gitlab.com/msevilla00/qgis_files)


### Módulo de introducción al scripting para análisis espacial

#### Scripting e introducción a Python

- Presentaciones y material de clase disponible en el directorio [python_moodle_2020](https://gitlab.com/msevilla00/unizar/-/tree/master/python_moodle_2020) de este repositorio

#### Introducción al tratamiento de datos en R

- Presentaciones y material de clase disponible en el directorio [R_moodle_2020]() (en construcción)
